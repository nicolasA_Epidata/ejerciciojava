/*
 * El metodo atacar recibira un personaje, y le pegara a la vida con el ataque
 * del personaje que ataca. El ataque tiene que ser filtrado por la defensa del
 * que recibe el golpe.
 * Devolvera el daño causado.
 */
public interface IAtacar {
	int atacar(Personaje personaje);
}
